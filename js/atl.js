jQuery(document).ready(function ($) {
// --- slider bike test
    $('.testSid .slide').bxSlider({
        preventDefaultSwipeX: true,
        slideWidth: 435,
        pager: false,
        nextText: ">",
        prevText: "<",
        preloadImages: 'all',
        controls: true
    });
// ---construccion de mosaico slider
    var htmlgrid = '';
    $('.gearSid .slide li a img').each(function () {
        htmlgrid += '<li><a href="' + $(this).attr('data-ref') + '"><img src="' + $(this).attr('src') + '" alt="" > </a></li>';
    });
    // --- html construct
    $('.gearSid .grid').html(htmlgrid);
    // --- slider gear
    $('.gearSid .slide').bxSlider({
        preventDefaultSwipeX: true,
        slideWidth: 435,
        pager: false,
        nextText: ">",
        prevText: "<",
        preloadImages: 'all',
        controls: true
    });
    // ---s/h mosaico gear slider
    $('.gearSid .tit .shMosaico').click(function () {
        $('.gearSid .grid').fadeToggle();
    });

    // toggle slide subseccion mosaico

    $(".ctngeneral .mosaico .tit .head").click(function () {
        $(this).next().slideToggle();
    });
    // --- open mosaico
    $(".testSid .tit .head").click(function () {
        $(this).next().slideToggle();
    });
});
