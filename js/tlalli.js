jQuery(document).ready(function ($) {
    $('.testSid .slide').bxSlider({
        preventDefaultSwipeX: true,
        slideWidth: 435,
        pager: false,
        nextText: ">",
        prevText: "<",
        preloadImages: 'all',
        controls: true
    });
});
  