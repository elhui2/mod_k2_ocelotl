jQuery(document).ready(function ($) {
    $('.videoSid .slide').bxSlider({
        preventDefaultSwipeX: true,
        slideWidth: 435,
        pager: false,
        nextText: ">",
        prevText: "<",
        preloadImages: 'all'
    });
});