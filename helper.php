<?php

/**
 * helper.php
 * Helper class for K2 Ocelotl module
 * @package	com.elhui2.ocelotl
 * @version 	0.2
 * @author 	Daniel Huidobro <contacto@elhui2.info> http://elhui2.info
 * @copyright 	Daniel Huidobro 2015.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html or later
 * */
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_SITE . DS . 'components' . DS . 'com_k2' . DS . 'helpers' . DS . 'route.php');

class k2Ocelotl {

    private $params;

    public function __construct($params) {
        $this->params = $params;
    }

    function sortItems() {
        $query = 'SELECT a.id AS id,a.catid AS catid,a.title AS title,a.introtext AS introtext,a.hits AS hits,a.alias AS alias,c.alias AS catalias FROM #__k2_items AS a LEFT JOIN #__k2_categories AS c ON ( a.catid = c.id ) WHERE (a.published = 1)';
        $cid = $this->params->get('category_id', null);
        if (!$this->params->get('catfilter')) {
            if (is_array($cid)) {
                if ($this->params->get('getChildren')) {
                    $itemListModel = K2Model::getInstance('Itemlist', 'K2Model');
                    $categories = $itemListModel->getCategoryTree($cid);
                    $sql = @implode(',', $categories);
                    $query .= " AND a.catid IN ({$sql})";
                } else {
                    JArrayHelper::toInteger($cid);
                    $query .= " AND a.catid IN(" . implode(',', $cid) . ")";
                }
            } else {
                if ($this->params->get('getChildren')) {
                    $itemListModel = K2Model::getInstance('Itemlist', 'K2Model');
                    $categories = $itemListModel->getCategoryTree($cid);
                    $sql = @implode(',', $categories);
                    $query .= " AND a.catid IN ({$sql})";
                } else {
                    $query .= " AND a.catid=" . (int) $cid;
                }
            }
        }

        if ($this->params->get('featureditems') == 0) {
            $query .= ' AND (a.featured != 0)';
        } else
        if ($this->params->get('featureditems') == 1) {
            $query .= '';
        } else
        if ($this->params->get('featureditems') == 2) {
            $query .= ' AND (a.featured = 1)';
        }


        $query .= ' ORDER BY a.' . $this->params->get('itemsOrdering') . ' DESC ' . 'LIMIT ' . $this->params->get('itemCount');

        $db = JFactory::getDBO();
        $db->setQuery($query);
        $articles = $db->loadObjectList();

        return $articles;
    }

    public function ehecatl($dataItems) {       
        $template = '<div class="ctnslideHome"><div class="slider"><ul class="slide">';
        $imgUrl = $this->params->get('cdnimages');
        if (empty($imgUrl)) {
            $imgUrl = rtrim(JURI::base(), '/');
        }

        foreach ($dataItems as $item) {
            $link = JRoute::_(K2HelperRoute::getItemRoute($item->id . ':' . urlencode($item->alias), $item->catid . ':' . urlencode($item->catalias)));
            $introtext = strip_tags($item->introtext);
            $template.='<li>'
                    . '<a href="' . $link . '">'
                    . '<img src="' . $imgUrl . '/media/k2/items/cache/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg" />'
                    . '<div class="ctntit">'
                    . '<p class="titulo">'
                    . '<a href="' . $link . '">' . $item->title . '</a>'
                    . '</p><span>' . $introtext . '</span>'
                    . '</div>'
                    . '</a>'
                    . '</li>';
        }
        $template.='</ul></div></div>';

        return $template;
    }

    public function tetl($dataItems) {

        $template = '<div class="videoSid">'
                . '<div class="tit"><h4>Videos</h4></div>'
                . '<div class="ctnslide">'
                . '<ul class="slide">';
        $imgUrl = $this->params->get('cdnimages');
        if (empty($imgUrl)) {
            $imgUrl = rtrim(JURI::base(), '/');
        }
        foreach ($dataItems as $item) {
            $link = JRoute::_(K2HelperRoute::getItemRoute($item->id . ':' . urlencode($item->alias), $item->catid . ':' . urlencode($item->catalias)));
            $template.='<li>'
                    . '<div class="share">'
                    . '<p class="titulo">' . $item->title . '</p>'
                    . '<div class="options">'
                    . '<a  class="w" href="whatsapp://send?text=' . rtrim(JURI::base(), '/') . $link . '" data-action="share/whatsapp/share"><span class="wa"></span></a>'
                    . '<a target="_blank" href="https://www.facebook.com/sharer.php?u=' . rtrim(JURI::base(), '/') . $link . '" class="f"><span class="icon-facebook"></span></a>'
                    . '<a target="_blank" href="https://twitter.com/share?url=' . rtrim(JURI::base(), '/') . $link . '" class="t" ><span class="icon-twitter"></span></a>'
                    . '</div>'
                    . '<a href="' . $link . '"><img src="' . $imgUrl . '/media/k2/items/cache/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg" alt="' . $item->title . '"></a>'
                    . '</li>';
        }
        $template.='</ul></div></div>';

        return $template;
    }

    public function tlalli($dataItems) {
        //Comprobar que la carpeta de cache exista o crearla
        if (!file_exists(JPATH_ROOT . '/cache/mod_k2_ocelotl')) {
            mkdir(JPATH_ROOT . '/cache/mod_k2_ocelotl', 0777, true);
        }

        //Importar la clase rezise-class.php
        include_once JPATH_ROOT . '/modules/mod_k2_ocelotl/class/resize-class.php';

        $template = '<div class="testSid">'
                . '<div class="tit"><h4>Bike Test</h4></div>'
                . '<div class="ctnslide">'
                . '<ul class="slide">';

        $imgUrl = $this->params->get('cdnimages');
        if (empty($imgUrl)) {
            $imgUrl = rtrim(JURI::base(), '/');
        }
        foreach ($dataItems as $item) {

            //Comprobar que las imagenes existan o crearlas
            if (file_exists(JPATH_ROOT . '/media/k2/items/cache/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg') && file_exists(JPATH_ROOT . '/cache/mod_k2_ocelotl/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg')) {
                $image = $imgUrl . '/cache/mod_k2_ocelotl/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg';
            } else
            if (file_exists(JPATH_ROOT . '/media/k2/items/cache/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg') && !file_exists(JPATH_ROOT . '/cache/mod_k2_ocelotl/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg')) {
                $rezise = new resize(JPATH_ROOT . '/media/k2/items/cache/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg');
                $rezise->resizeImage(320, 320, $option = 'crop');
                $rezise->saveImage(JPATH_ROOT . '/cache/mod_k2_ocelotl/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg', 75);
                $image = $imgUrl . '/cache/mod_k2_ocelotl/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg';
            }

            $link = JRoute::_(K2HelperRoute::getItemRoute($item->id . ':' . urlencode($item->alias), $item->catid . ':' . urlencode($item->catalias)));

            $template.='<li>'
                    . '<a href="' . $link . '">'
                    . '<img src="' . $image . '" alt="' . $item->title . '">'
                    . '<div class="ctntit">'
                    . '<p class="titulo">' . $item->title . '</p>'
                    . '</div>'
                    . '</li>';
        }
        $template.='</ul></div></div>';

        return $template;
    }
    
    public function atl($dataItems){
        //Comprobar que la carpeta de cache exista o crearla
        if (!file_exists(JPATH_ROOT . '/cache/mod_k2_ocelotl')) {
            mkdir(JPATH_ROOT . '/cache/mod_k2_ocelotl', 0777, true);
        }

        //Importar la clase rezise-class.php
        include_once JPATH_ROOT . '/modules/mod_k2_ocelotl/class/resize-class.php';

        $template = '<div class="gearSid">'
                . '<div class="tit"><h4>Bike Test</h4><a class="shMosaico"></div>'
                . '<div class="ctnslide">'
                . '<ul class="slide">';

        $imgUrl = $this->params->get('cdnimages');
        if (empty($imgUrl)) {
            $imgUrl = rtrim(JURI::base(), '/');
        }
        
        foreach ($dataItems as $item) {

            //Comprobar que las imagenes existan o crearlas
            if (file_exists(JPATH_ROOT . '/media/k2/items/cache/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg') && file_exists(JPATH_ROOT . '/cache/mod_k2_ocelotl/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '_atl.jpg')) {
                $image = $imgUrl . '/cache/mod_k2_ocelotl/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '_atl.jpg';
            } else
            if (file_exists(JPATH_ROOT . '/media/k2/items/cache/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg') && !file_exists(JPATH_ROOT . '/cache/mod_k2_ocelotl/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '_atl.jpg')) {
                $rezise = new resize(JPATH_ROOT . '/media/k2/items/cache/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '.jpg');
                $rezise->resizeImage(320, 320, $option = 'crop');
                $rezise->saveImage(JPATH_ROOT . '/cache/mod_k2_ocelotl/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '_atl.jpg', 75);
                $image = $imgUrl . '/cache/mod_k2_ocelotl/' . md5('Image' . $item->id) . '_' . $this->params->get('sizeimages') . '_atl.jpg';
            }

            $link = JRoute::_(K2HelperRoute::getItemRoute($item->id . ':' . urlencode($item->alias), $item->catid . ':' . urlencode($item->catalias)));
            
            $template.='<li>'
                    . '<a href="' . $link . '">'
                    . '<img src="' . $image . '" alt="' . $item->title . '" data-ref="' . $link . '">'
                    . '</a>'
                    . '<div class="ctntit">'
                    . '<p class="titulo">' . $item->title . '</p>'
                    . '</div>'
                    . '</li>';
        }
        $template.='</ul></div><ul class="grid"></ul></div>';

        return $template;        
    }

}
