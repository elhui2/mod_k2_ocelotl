<?php
$doc->addStyleSheet(JURI::root(true) . '/modules/mod_k2_ocelotl/css/jquery.bxslider.min.css');
$doc->addScript(JURI::root(true) . '/modules/mod_k2_ocelotl/js/jquery.bxslider.min.js', 'text/javascript" defer async');

$template = $params->get('ocelotlTemplate');
switch ($params->get('ocelotlTemplate')){
    case 'ehecatl':
        $doc->addScript(JURI::root(true) . '/modules/mod_k2_ocelotl/js/ehecatl.min.js','text/javascript" defer async');
        echo $k2Ocelotl->ehecatl($items);
        break;
    case 'tletl':
        $doc->addScript(JURI::root(true) . '/modules/mod_k2_ocelotl/js/tetl.min.js','text/javascript" defer async');
        echo $k2Ocelotl->tetl($items);
        break;
    case 'tlalli':
        $doc->addScript(JURI::root(true) . '/modules/mod_k2_ocelotl/js/tlalli.min.js','text/javascript" defer async');
        echo $k2Ocelotl->tlalli($items);
        break;
    case 'atl':
        $doc->addScript(JURI::root(true) . '/modules/mod_k2_ocelotl/js/atl.min.js','text/javascript" defer async');
        echo $k2Ocelotl->atl($items);
        break;
}
