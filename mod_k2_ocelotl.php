<?php

defined('_JEXEC') or die('Restricteted access');

require_once dirname(__FILE__) . '/helper.php';
$doc = Jfactory::getDocument();
$k2Ocelotl = new k2Ocelotl($params);
$items = $k2Ocelotl->sortItems();

require(JModuleHelper::getLayoutPath('mod_k2_ocelotl'));
